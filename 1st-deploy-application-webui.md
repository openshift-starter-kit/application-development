# はじめてのアプリケーションデプロイ (OpenShift Web Console)
このハンズオンでは OpenShift Web Console からサンプルの Python アプリケーションを用いて、コンテナイメージをビルドし、アプリケーション Pod をデプロイします。
次に Liveness Probe によるヘルスチェックの設定や Pod のオートヒールを確認します。

---
## アジェンダ
- [はじめてのアプリケーションデプロイ (OpenShift Web Console)](#はじめてのアプリケーションデプロイ-openshift-web-console)
  - [アジェンダ](#アジェンダ)
  - [1. OpenShift Console へのログイン](#1-openshift-console-へのログイン)
  - [2. アプリケーションのデプロイ](#2-アプリケーションのデプロイ)
    - [2.1 プロジェクトの作成](#21-プロジェクトの作成)
    - [2.2 Podのデプロイ](#22-podのデプロイ)
    - [2.3 動作確認](#23-動作確認)
  - [3. Liveness Probe 設定の追加](#3-liveness-probe-設定の追加)
  - [4. Liveness Probe によるヘルスチェック](#4-liveness-probe-によるヘルスチェック)
  - [5. Pod のスケールアウト](#5-pod-のスケールアウト)

---
## 1. OpenShift Console へのログイン
Webブラウザより OpenShift Web Console へアクセスします。

**操作 1**
アカウントとパスワードを入力し、ログインします。
![Console Login](./images/WS001.png)

ログインが完了すると、Developer View が表示されます。

**操作 2**
ツアーガイドが表示される場合、スキップします.
画面右上の `?` アイコンから"ガイド付きツアー"を呼び出すことでからいつでも表示できます.
![Console Login](./images/WS002.png)

* **参考**: ガイド付きツアー を任意に実行する例になります。
![Console Login](./images/WS003-3.png)

* **参考**: ２回目以降のログイン等の事情で、Administrator ビューが表示される場合は、左上部分のメニューより切り替えを行ってください。

![Console Login](./images/WS003-2.png)

---
## 2. アプリケーションのデプロイ
### 2.1 プロジェクトの作成
最初に、プロジェクトを <アカウント>-webapp の名前で作成します。プロジェクトは、OpenShiftを操作する過程で作成するリソース等を格納する場所になります。プロジェクトの名前は、他受講者との重複を避けるために、例えば、アカウントが `user1` の場合、`user1-webapp` の様に、アカウント名を含めた名前として下さい。

**操作 3**
画面中央部の "プロジェクト:すべてのプロジェクト" と表示されているドロップダウンリストを選択し、リスト最下部の "プロジェクト作成" ボタンを押下します。
![Console Login](./images/WS004-2.png)

**操作 4**
プロジェクトの作成ウィンドウにて、"名前" フィールドにプロジェクト名を入力し、作成ボタンを押下します。
![Console Login](./images/WS006-2.png)

**確認**
画面が切り替わった際、"プロジェクト:アカウント-webapp" となっていることを確認します。
※操作の対象が、表示されているProject ( namespace ) であることを意味しています。
![Console Login](./images/WS007-3.png)

### 2.2 Podのデプロイ
本節では、Python カタログ(コンテナイメージをGUI上から簡単に使えるように登録された情報)を使用して、コンテナイメージをビルドし、アプリケーション Pod をデプロイします。

**操作 5**
左メニューから、"+追加" を選択します。
既に選択されている場合は、スキップします。
![Console Login](./images/WS007-5.png)

**操作 6**
"すべてのサービス" を押下します。
![Console Login](./images/WS007-4.png)

**操作 7**
"開発カタログ"画面の"キーワードでフィルター"に `python` と入力し、カタログのリスとにフィルターを適用します。
![Console Login](./images/WS008.png)

**操作 8**
ビルダーイメージ：Pythonを押下します。この操作により、新たにコンテナイメージを作成する際に、Python実行が可能な状態になっているコンテナイメージを使用することを指定したことになります。
![Console Login](./images/WS008-2.png)

**操作 9**
カタログの説明が表示されるので、"アプリケーションの作成"ボタンを押下します。
![Console Login](./images/WS009.png)

**操作 10**
Source-to-Image(S2I) アプリケーションの作成　画面の GitリポジトリーURLに、
"https://github.com/openshift-katacoda/blog-django-py"
と入力し、画面下部の作成ボタンを押下します。この操作により、先程指定したPython実行が可能な状態になっているコンテナイメージ上に、上記Gitリポジトリ上のPythonソースコードがマージされる形で、新規にコンテナイメージが作成する指定をし、コンテナイメージビルド、及びデプロイを開始します。
![Console Login](./images/WS010-2.png)

**確認**
画面がトポロジービューに変わります。デプロイ処理が完了するまで待ちとなります。
![Console Login](./images/WS011.png)

次に、デプロイ処理中の状態を確認します。Console の画面を探索し、どのような情報が参照できるか確認します。
**当セクションの操作はデプロイ処理の状態により表示が異なり、資料中のスクリーンショットはデプロイ処理中に順次処理されていく処理を取り上げていますが、例えばビルド処理が完了下後のビルドログの表示等が異なる場合があります。**

* Deploy ステータスの確認
表示されているアイコンを押下すると、右側に Deploymentの定義情報等を表示する画面が表示されます。
![Console Login](./images/WS012-2.png)

* リソースの状態の確認
Deployment 情報表示部の内部タブ"リソース"を選択すると、リソースの状態を確認できます。
この資料の画像は、ビルド処理中となります。
![Console Login](./images/WS013-2.png)

* ビルドログの確認
リソースタブ内の"ビルド"セクションにある"ログの表示"リンクを押下すると、ビルドログを確認することができます。
![Console Login](./images/WS014-3.png)

* ビルドログの出力画面
![Console Login](./images/WS014.png)

* デプロイの完了
Pod のデプロイ処理が完了すると、トポロジービューのアイコンの表示が、濃い青枠で囲まれます。
![Console Login](./images/WS015.png)

### 2.3 動作確認
ブラウザを介してアクセスすることが可能となりますので、これにより、動作確認を行います。

補足: Podの外部公開は、`Route` を作成することで設定可能です。`Route`は、OpenShiftリソースの1つであり、このRouteを作成することで、OpenShiftが、自動的に、作成したPodをOpenShiftクラスタ外部へ公開する設定を行います。

**操作 11**
トポロジービューのアプリケーションアイコン右上の記号を押下します。
![Console Login](./images/WS016.png)

**確認**
サンプルアプリケーションの画面が表示されることを確認します。
 ※アプリケーションの作成操作から、10分以上経過してもアプリケーションが起動してこない場合は、講師へお声がけください。
![Console Login](./images/WS017.png)

---
## 3. Liveness Probe 設定の追加
コンテナアプリのヘルスチェック設定を行ないます。

**操作 12**
トポロジービューにて、Deployment 情報画面を開き（Pod のアイコンを押下 )、情報画面の上部に表示されている
"ヘルスチェック"セクションにある "ヘルスチェックの追加"リンクを押下します。
![Console Login](./images/WS015-2.png)

**操作 13**
"Liveness Probe の追加" ボタンを押下します。
![Console Login](./images/WS018-2.png)

**操作 14**
(今回はデフォルトの設定を使用します。)展開されたセクションの最下部にある"ㇾ"チェックを押下します。
![Console Login](./images/WS019-1.png)
(画面の続き)
![Console Login](./images/WS019-4.png)

**操作 15**
Liveness Probe の表示が、"追加済みのLiveness Probe" (緑色の文字)となっていることを確認し、画面下部の"追加" ボタンを押下します。
![Console Login](./images/WS019-5.png)

**確認**
Pod の再デプロイ処理が実行されるので、完了するのを待ちます。
![Console Login](./images/WS020.png)

---
## 4. Liveness Probe によるヘルスチェック
コンテナ内で擬似的に障害を発生させ、コンテナが復旧されることを確認します。
コンテナへターミナルアクセスし、アプリケーションの構成ファイルを削除すると、コンテナは HTTP リターンコード 403 を返しますが、数秒後に Liveness Probe により自動的にコンテナは復旧されます。
前節の Liveness Probe の設定が済んでない場合は、ここでの操作を行なったとしても回復は行なわれないので注意します。

**確認**
トポロジービューのアプリケーションアイコン右上の記号から、アプリケーションへアクセスし、Blog アプリの画面が表示することを確認します。
![Console Login](./images/WS017.png)

**操作 16**
OpenShift のコンソールに戻り、トロポジービューから Deployment の情報を開きます。
続けて Deployment 情報の Pod セクションから Pod 名（青文字表記のリンク)を押下します。
![Console Login](./images/WS015-3.png)

**操作 17**
Pod の詳細画面にて、"ターミナル"タブを選択します。
![Console Login](./images/WS021-2.png)

**操作 18**
擬似的に障害をおこします。ターミナルのシェル(コマンドライン)にて、下記のコマンドを入力します。
```
# カレントディレクトリが上記であることを確認します。
$ pwd 
/opt/app-root/src  

# ファイルやディレクトリがあることを確認します。
$ ls
app.sh blog ...

# ファイルやディレクトリを削除します。
$ rm -rf *
$ ls
```

![Console Login](./images/WS022.png)
![Console Login](./images/WS023.png)

**確認**
Blog アプリケーションへアクセスし、"Forbidden (HTTP 403 )" が返されることを確認します。
![Console Login](./images/WS024.png)

**確認**
10秒程度の間隔で画面をリフレッシュして、アプリケーションの画面表示が正常になることを確認します。
![Console Login](./images/WS025.png)

次に障害検出から回復までのイベントを確認します。

**操作 19**
OpenShift Console のトポロジービューより、Deployment の情報画面を展開し、PodセクションにあるPodのリンクをたどります。
![Console Login](./images/WS015-3.png)

**操作 20**
Pod の詳細画面にて"イベント"タブを押下します。
![Console Login](./images/WS021-3.png)

**確認**
Pod のイベント一覧の中に、Liveness Prove の失敗や、その後のリスタートのイベントが記録されていることを確認します。
表示の内容から、障害が検出され、回復が行なわれたことが分かります。
![Console Login](./images/WS028.png)

---
## 5. Pod のスケールアウト
Pod のレプリカ数を変更し、Pod がスケールアウトされ、Pod のスケールに応じて Service リソースのエントリーが自動的に更新されることを確認します。

**操作 21**
トポロジービューにてアプリケーションのアイコンを選択し、Deployment詳細の画面を開きます。
![Console Login](./images/WS029-2.png)

**操作 22**
Deployment詳細画面の "^"ボタンを1回押下し、Pod のスケールを+1 します。
![Console Login](./images/WS030.png)

**確認**
スケール処理が完了すると、濃い青円の中の数字が2となります。

* Pod のスケール調整中
![Console Login](./images/WS031-2.png)

* Pod のスケール調整完了
![Console Login](./images/WS032-2.png)

**<<注意>>**操作 23 〜 25 では動作の確認を行なうためにクライアントセッションのスティッキーを解除する操作を行ないます。通常のアプリケーションではクライアントセッションのスティッキー設定は要件を考慮し適切に設定されている必要があります。

**操作 23**
コマンドラインターミナル(OCP4.9ではTech pre-view)を起動するため、画面最上部、右部分の `>_` アイコンを押下します。
コマンドラインターミナルのアイコンが表示されない場合、Web 端末 (Web Terminal Operator) をインストールする必要があります。インストールの手順は https://access.redhat.com/documentation/ja-jp/openshift_container_platform/4.10/html-single/web_console/index#odc-about-web-terminal を参照して下さい。
![Console Login](./images/WS033-2.png)

**操作 24**
画面下部にコマンドラインターミナル起動のウィザードが表示される。プロジェクト名を"userxx-webapp"とし、開始ボタンを押下します。
![Console Login](./images/WS034-2.png)

**確認**
コマンドラインターミナルの起動が完了すると、Shellプロンプトが表示されます。
![Console Login](./images/WS036-2.png)

**操作 25**
コマンドラインターミナルから下記のコマンドを入力します。
```
export USER=$(oc whoami)
oc project ${USER}-webapp
oc annotate routes blog-django-py haproxy.router.openshift.io/balance='roundrobin'
oc annotate routes blog-django-py haproxy.router.openshift.io/disable_cookies='true'
```
![Console Login](./images/WS037.png)

**確認**
サンプルアプリケーションの画面をリフレッシュし、接続先のPod がアクセス毎に交互になることを確認します。
![Console Login](./images/WS038-3.png)

次に Service リソースに対し、スケールと同数の Pod がリストアップされていることを確認します。

**操作 26**
トポロジービューにて、サンプルアプリケーション(①)のDeployment詳細画面内のリソースタブ(②)を選択し、セクション内のservice のリンク(③)を押下します。
![Console Login](./images/WS039-2.png)

**操作 27**
service の詳細画面にて、"Pod"タブを押下します。
![Console Login](./images/WS040-2.png)

**確認**
Pod の一覧として、スケールと同数のPod がリストアップされていることを確認します。
![Console Login](./images/WS041-2.png)

以上でハンズオンは終了です。
