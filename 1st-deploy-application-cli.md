# はじめてのアプリケーションデプロイ (OpenShift CLI)
このハンズオンでは OpenShift Web Console からサンプルの Python アプリケーションを用いて、コンテナイメージをビルドし、アプリケーション Pod をデプロイします。

---
## アジェンダ
- [はじめてのアプリケーションデプロイ (OpenShift CLI)](#はじめてのアプリケーションデプロイ-openshift-cli)
  - [アジェンダ](#アジェンダ)
  - [1. OpenShift Console へのログイン](#1-openshift-console-へのログイン)
  - [2. アプリケーションのデプロイ](#2-アプリケーションのデプロイ)
    - [2.1 プロジェクトの作成](#21-プロジェクトの作成)
    - [2.2 BuildConfigとImageStreamの作成、及びイメージ作成](#22-buildconfigとimagestreamの作成及びイメージ作成)
    - [2.3 Podのデプロイ](#23-podのデプロイ)
    - [2.４ Routeの作成](#2４-routeの作成)
  - [3. Podのスケールアウト](#3-podのスケールアウト)
  - [４. Liveness Probe 設定の追加](#４-liveness-probe-設定の追加)
    - [4.1 疑似障害の生成](#41-疑似障害の生成)
    - [4.2 Liveness Probeの設定](#42-liveness-probeの設定)
    - [4.3 動作確認](#43-動作確認)

---
## 1. OpenShift Console へのログイン
OpenShift CLI (`oc` コマンド) を用いて OpenShift Console にログインします。本演習では、`Web Terminal`と呼ばれるブラウザ上から操作可能な端末を使用しますので、ブラウザよりOpenShift Web Consoleにアクセスし、ログイン後、画面右上の`>_`アイコンをクリックし、Web Terminalを起動します。

> 補足: 初回起動時は、少々時間がかかりますので、お待ち下さい。

![picture 1](images/89c24f73e5524ce0ed85e37f311b05a4896eeed621903b12ee88488f25e065f6.png)  

Web Terminalを使用する場合、既にログイン済みの状態となりますが、練習のため、以下コマンドにより、logoutします。
```
oc logout
```

次に、以下コマンドにより、ログインします。ユーザ名を自分のユーザ名に置き換えて、コマンド実行してください。

```
oc login -u <ユーザ名> -p openshift <OpenShift API URL>
```

以下、上記コマンドの説明です。
- oc login
  - OpenShift環境へのログインコマンド
- -u <ユーザー名>
  - ログインユーザー名の指定
- -p <パスワード>
  - パスワードの指定
- \<OpenShift API URL>
  - OpenShiftの公開するAPIのURLを指定
  - 本演習環境では、`oc whoami --show-server`コマンドにより取得可能です。

`Login successful.`のメッセージが出力されれば、ログイン成功です。

---
## 2. アプリケーションのデプロイ
### 2.1 プロジェクトの作成
プロジェクトを <アカウント>-cli の名称で作成します。例えばアカウントが `user1` の場合、`user1-cli` とします。
```sh
oc new-project $(oc whoami)-cli
```

ここでは、使用ユーザー名を取得する`oc whoami`コマンドを使用して、ユーザー名を動的に指定しています。

また、プロジェクト作成済みの場合に、使用プロジェクトを指定するには以下のコマンドを実行します。
```sh
oc project $(oc whoami)-cli
```

> Web Terminalの場合、ブラウザからの操作が数10分間未発生の場合、ブラウザからWeb Terminalへの再接続が行われます。その際、プロジェクトがデフォルトのプロジェクトに変更されますので、再接続の際は、使用プロジェクトにご注意下さい。

### 2.2 BuildConfigとImageStreamの作成、及びイメージ作成
本節では、`oc new-build`コマンドを使用して、s2iによるコンテナイメージ作成を行います。

```
oc new-build python:3.9-ubi8~https://github.com/openshift-katacoda/blog-django-py.git -n $(oc whoami)-cli
```

`oc new-build`コマンドは、コンテナイメージを作成する目的で使用するコマンドであり、様々なオプションが存在します。上記コマンドの様に、`ベースイメージ(=python:3.9-ubi8という名のImageStream)` + `~` + `PythonソースコードURL(=https://github.com/openshift-katacoda/blog-django-py.git)`のオプション形式を使用した場合、コンテナイメージを作成するために必要となる以下リソースを自動生成した上で、コンテナイメージ作成を開始します。
- BuildConfig(短縮表記: bc)
- ImageStream(短縮表記: is)

また、-nオプションは、使用するprojectを指定するオプションです。ocコマンドは、現在作業中のprojectをデフォルトで使用するので、基本的に指定不要ですが、使用中のprojectの間違いに起因する作業ミスを回避するために、可能な限り、-nオプションを使用することを推奨致します。

上記リソースの生成は、例えば、以下コマンドにより確認可能です。
```
oc get bc -n $(oc whoami)-cli
oc get is -n $(oc whoami)-cli
```

以下、上記コマンドの説明です。
- oc get <リソースタイプ>
  - リソースタイプで指定したリソースを取得するコマンド
- bc/is
  - リソースタイプであるBuildConfig/ImageStreamの短縮形表記

`oc new-build`コマンド実行した後、コンテナイメージのビルドを行うためのPodが起動します。以下コマンドにより、ビルド用のPodが起動していることを確認します。

```
oc get pod -n $(oc whoami)-cli
```

出力例
```
bash-3.2$ oc get pod
NAME                     READY   STATUS     RESTARTS   AGE
blog-django-py-1-build   0/1     Init:0/2   0          3s
```

ビルド用Podの具体的な処理を確認するために、以下`oc logs`コマンドによりログを出力します。<Pod名>には、上記oc get podで出力されるNAMEカラムの値(上記出力例の場合、blog-django-py-1-build)を使用して下さい。また、`-f`オプションは、ログの監視表示(リアルタイム表示)を有効にするオプションです。

```
oc logs -f <Pod名> -n $(oc whoami)-cli
```

以下の様な、コンテナイメージのビルドが進行中であるログが出力されること、及び、最終的に、`Push successful`のメッセージが出力されることを確認します。

```
<省略>
Pushing image image-registry.openshift-image-registry.svc:5000/user1-cli/blog-django-py:latest ...
Getting image source signatures
Copying blob sha256:628c47f182e09681d4cdfa27caf437ba2adbabb5e9a3d4222a5cb3f45de8fc2e
Copying blob sha256:436bf753eb551f22d718c0fa7103efc29841a1f958cef5a29525ce8f7660ae0b
Copying blob sha256:354c079828fae509c4f8e4ccb59199d275f17b0f26b1d7223fd64733788edf32
Copying blob sha256:7e3624512448126fd29504b9af9bc034538918c54f0988fb08c03ff7a3a9a4cb
Copying blob sha256:e0dc1b5a4801cf6fec23830d5fcea4b3fac076b9680999c49935e5b50a17e63b
Copying blob sha256:db0f4cd412505c5cc2f31cf3c65db80f84d8656c4bfa9ef627a6f532c0459fc4
Copying config sha256:2b8e0cee4a65fee23336a383d1cdb65c885afec063abccfecd2fef090549f1f3
Writing manifest to image destination
Storing signatures
Successfully pushed image-registry.openshift-image-registry.svc:5000/user1-cli/blog-django-py@sha256:d27944522b833238cab27994d5390b41f81781ac3db1efd94f7df32a95f68205
Push successful
```

次に、作成されたBuildConfigの設定内容を以下コマンドにより出力します。

```
oc get bc blog-django-py -o yaml -n $(oc whoami)-cli
```

>
> `blog-django-py`は、BuildConfig名であり、`oc get bc -n $(oc whoami)-cli`で出力されるNAMEカラムの値となります。

また、以下設定内容を確認します。
- 使用するベースイメージとして、`oc new-build`で指定したImageStreamが、設定されているか?
  - `spec.strategy.sourceStrategy.from`を参照
- 使用するソースコードとして、`oc new-build`で指定したURLが、設定されているか?
  - `spec.source.git.url`を参照
- 作成するイメージを参照するImageStream名は?
  - `spec.output.to`を参照



出力例
```yaml
apiVersion: build.openshift.io/v1
kind: BuildConfig
metadata:
  annotations:
    openshift.io/generated-by: OpenShiftNewBuild
  creationTimestamp: "2022-09-05T14:44:19Z"
  generation: 2
  labels:
    build: blog-django-py
  name: blog-django-py
  namespace: user1-cli
  resourceVersion: "277935"
  uid: 52b8472b-c59b-4e75-b559-853d7fbd8952
spec:
  failedBuildsHistoryLimit: 5
  nodeSelector: null
  output:
    to:
      kind: ImageStreamTag
      name: blog-django-py:latest
  postCommit: {}
  resources: {}
  runPolicy: Serial
  source:
    git:
      uri: https://github.com/openshift-katacoda/blog-django-py.git
    type: Git
  strategy:
    sourceStrategy:
      from:
        kind: ImageStreamTag
        name: python:3.9-ubi8
        namespace: openshift
    type: Source
  successfulBuildsHistoryLimit: 5
  triggers:
  - github:
      secret: mETdCrnf-e8gpx0u1oh1
    type: GitHub
  - generic:
      secret: iD61OUzS5yQ9Q7qHmRsi
    type: Generic
  - type: ConfigChange
  - imageChange: {}
    type: ImageChange
status:
  imageChangeTriggers:
  - from:
      name: python:3.9-ubi8
      namespace: openshift
    lastTriggerTime: null
    lastTriggeredImageID: image-registry.openshift-image-registry.svc:5000/openshift/python@sha256:10fec53146bf86bcdc0735a1b17584413c4559d312776b306cbd032406dd24a7
  lastVersion: 1
```

同様に、作成されたImageStreamの設定内容を以下コマンドにより出力します。

```
oc get is blog-django-py -o yaml -n $(oc whoami)-cli
```

以下の様な出力を確認し、`latest`という名前のtagにより、`dockerImageReference`でポイントされるコンテナイメージが参照される設定になっていることを確認します。

出力例
```yaml
status:
  dockerImageRepository: image-registry.openshift-image-registry.svc:5000/user1-cli/blog-django-py
  publicDockerImageRepository: default-route-openshift-image-registry.apps.cluster-zhgp8.zhgp8.sandbox1823.opentlc.com/user1-cli/blog-django-py
  tags:
  - items:
    - created: "2022-09-06T02:59:43Z"
      dockerImageReference: image-registry.openshift-image-registry.svc:5000/user1-cli/blog-django-py@sha256:d8d045500a3e8f6c7daf52e608f813085677d5e280e5730715f01280f4717330
      generation: 1
      image: sha256:d8d045500a3e8f6c7daf52e608f813085677d5e280e5730715f01280f4717330
    tag: latest
```    


### 2.3 Podのデプロイ
本節では、前節においてs2iを使用して作成したPythonアプリのコンテナイメージのデプロイを行います。

最初に、以下コマンドにより、前節で作成したPythonアプリのImageStreamを使用して、デプロイを行います。

```
oc new-app blog-django-py -n $(oc whoami)-cli
```

`oc new-app`コマンドは、Podを起動する目的で使用するコマンドであり、様々なオプションが存在します。上記コマンドの様に、`ImageStream名(=blog-django-py)`を指定した場合、Podを起動するために必要となる以下リソースを自動生成します。
- Deployment(短縮表記: deploy)
- Service(短縮表記: svc)
- ReplicaSet(短縮表記: rs)

上記リソースの生成は、例えば、以下コマンドにより確認可能です。
```
oc get deploy -n $(oc whoami)-cli
oc get svc -n $(oc whoami)-cli
oc get rs -n $(oc whoami)-cli
```

> 補足: 使用できる登録済みアプリケーションテンプレートの一覧は `oc new-app --list` で取得することが可能です。

次に、以下コマンドにより、Podが作成されたこと、及び、`STATUS`が`Running`になることから、正常に起動したことを確認します。

```sh
oc get pod -n $(oc whoami)-cli
```

出力例(`blog-django-py-` 以降の ID は環境に応じて異なります)
```
NAME                              READY   STATUS      RESTARTS   AGE
blog-django-py-1-build            0/1     Completed   0          4m8s
blog-django-py-xxxxxxx-xxxxxxxx   1/1     Running     0          41s
```

### 2.４ Routeの作成
Routeは、OpenShiftリソースの1つであり、Route作成により、OpenShiftの管理するRouter(HAProxy)の設定変更が行われ、起動したPodアプリケーションをクラスタの外部に公開することを可能とします。

最初に、以下コマンドにより、Serviceを使用してRouteを作成します。

```sh
oc expose svc/blog-django-py -n $(oc whoami)-cli
```

以下コマンドにより Route作成により設定された外部公開用URLを確認します。
```sh
oc get route -n $(oc whoami)-cli
```

出力例
```
NAME             HOST/PORT                                                                    PATH   SERVICES         PORT       TERMINATION   WILDCARD
blog-django-py   blog-django-py-user*-cli.apps.cluster-*****.*****.sandbox****.opentlc.com           blog-django-py   8080-tcp                 None
```

上記の結果を基ににブラウザから `http://<<HOST>>` にアクセスします。

> 注意: `https`ではなく、`http`ですので、ご注意下さい。

![Console Login](./images/WS017.png)

以上で、アプリケーションのデプロイは、完了です。

## 3. Podのスケールアウト
本章では、Pod のレプリカ数を変更し、Pod がスケールアウトされること、Pod のスケールに応じて、Routeリソースのエントリーが、OpenShift内部で自動的に更新されることを確認します。

最初に、以下コマンドにより、Pod レプリカ数を 2 に設定します。
```sh
oc scale deployment/blog-django-py --replicas=2 -n $(oc whoami)-cli
```

以下コマンドにより、Podの数が2つになっていることを確認します。
```
oc get pod -n $(oc whoami)-cli
```

出力例
```
bash-3.2$ oc get pod -n $(oc whoami)-cli
NAME                              READY   STATUS      RESTARTS   AGE
blog-django-py-1-build            0/1     Completed   0          130m
blog-django-py-5ddb5d559f-9xnzw   1/1     Running     0          61m
blog-django-py-5ddb5d559f-njmvc   1/1     Running     0          47s
```

次に、スケールした2つのPodへのネットワーク疎通を確認するために、以下コマンドにより、Routeに対して、Round Robin用のAnnotation、及び、Cookie無効化用のAnnotationを付与します。
```
# 各 Pod へのアクセスを Round Robin で設定します。
oc annotate routes blog-django-py haproxy.router.openshift.io/balance='roundrobin' -n $(oc whoami)-cli

# アクセス先をセッションごとに固定するため Cookie を無効化します。
oc annotate routes blog-django-py haproxy.router.openshift.io/disable_cookies='true' -n $(oc whoami)-cli
```

次に、ブラウザ上にて、サンプルアプリケーションの画面をリフレッシュし、接続先のPod がアクセス毎に交互になることを確認して下さい。

> Routeの設定反映完了に、若干時間がかかりますので、ご留意下さい。

![Console Login](./images/WS038-3.png)

以上で、Pod のスケールアウトは、完了です。次章の演習のために、以下コマンドにより、Pod数を1に戻して下さい。
```
oc scale deployment/blog-django-py --replicas=1 -n $(oc whoami)-cli
```

## ４. Liveness Probe 設定の追加
本章では、コンテナ内で擬似的に障害を発生させた上で、Liveness Probeが設定されている場合と未設定の場合におけるPodの動作を確認することで、Liveness Probeの理解を深めることを目的とします。

### 4.1 疑似障害の生成
本節では、コンテナ内で擬似的に障害を発生させ、アプリへのアクセスが失敗(HTTP Statusが403)になることを確認します。


最初に、以下コマンドにより、`oc rsh` を使用して、コンテナに Shell でアクセスします。
```sh
oc rsh $(oc get pod -l deployment=blog-django-py -o name  -n $(oc whoami)-cli) bash
```

以下、上記コマンドの説明になります。
- `oc rsh <Pod名> <コマンド名>`
  - Pod名で指定したPodに対して、コマンド名で指定したコマンドを実行します。
- `oc get pod -l <ラベル名>`
  - Podに付与されるラベルを指定して、取得したいPodを選定するためのオプションです。
- `-o name`
  - `oc get`でのoutput形式を指定します。`name`は、`oc get`で取得するリソースの名前のみを出力する形式です。

次に、擬似的に障害を起こします。具体的には、Pythonのソースコードを削除することで、疑似障害を起こします。ターミナルのシェル(コマンドライン)にて、下記のコマンドを入力します。

```
rm -rf /opt/app-root/src/*
```

次に、以下コマンドにより、bashを抜けます。
```
exit
```

確認: Blog アプリケーションへアクセスし、"Forbidden (HTTP 403 )" が返されることを確認します。
![Console Login](./images/WS024.png)

### 4.2 Liveness Probeの設定
OpenShiftでは、KubernetesのLiveness Probeの機能を使用可能です。Kubernetesは、このLiveness Probeの機能により、Podでの異常検知をし、Podを自動的に再起動することが可能であり、結果として、システムの安定稼働を実現します。

Liveness Probeの機能を利用するためには、KubernetesからのLiveness Probeに対して、コンテナにおける正常性/異常性を返信するための設定が必要となります。本節では、Pythonアプリに対して、Liveness Probe用の設定を行います。

最初に、以下コマンドにより、追加するLiveness Probe用設定を記述したYAML形式ファイル(livenessProbe.yml)を作成します。

```sh
cat <<EOF > livenessProbe.yml
spec:
  template:
    spec:
      containers:
      - name: blog-django-py
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 8080
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
EOF
```

次に、以下コマンドにより、上記Liveness Probeの設定を以下の手順で現状のDeploymentに適用(Patch)します。

```sh
oc patch deployment/blog-django-py --patch-file ./livenessProbe.yml -n $(oc whoami)-cli
```

以下コマンドにより、適用後のDeployment定義を確認し、livenessProbeの設定が追加されていることを確認します。

```
# 変更後のデプロイメントの状態を確認
oc get deploy/blog-django-py -o yaml -n $(oc whoami)-cli
```

出力例
```yaml
    spec:
      containers:
      - image: image-registry.openshift-image-registry.svc:5000/user1-cli/blog-django-py@sha256:d8d045500a3e8f6c7daf52e608f813085677d5e280e5730715f01280f4717330
        imagePullPolicy: IfNotPresent
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 8080
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
```          

### 4.3 動作確認
Liveness Probeにより、コンテナが再起動され、元の状態に戻ることを確認します。

操作: 10秒程度の間隔で画面をリフレッシュして、アプリケーションの画面表示が正常になることを確認します。
![Console Login](./images/WS025.png)

また、以下コマンドにより、出力されるPod情報のEventsセクションから、Liveness probe が失敗し、コンテナが再作成されることを確認します。
```sh
oc describe $(oc get pod -l deployment=blog-django-py -o name) -n $(oc whoami)-cli
```

出力例
```
 Warning  Unhealthy       3m47s (x3 over 4m7s)  kubelet            Liveness probe failed: HTTP probe failed with statuscode: 403
  Normal   Killing         3m47s                 kubelet            Container blog-django-py failed liveness probe, will be restarted
```  

> ご参考:
> Event情報は、以下コマンドでも取得可能です。エラー解析に有用ですので、ご参照下さい。
```
oc get event --sort-by .lastTimestamp -n $(oc whoami)-cli
```

以上でハンズオンは終了です。
